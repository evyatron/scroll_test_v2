Hello The Secret Police!

I ended up going with the solution I found the most obvious and easy to implement - only **instantiating prefabs on demand, and hiding out-of-view game objects**.

### Things to note:
1. I removed a Thumbnail component from the Content GameObject, I believe it was there by mistake
2. I removed the Size Fitter component from the Content GameObject since I now set it at runtime
3. I've added tan OnValueChanged listener to the ScrollView GameObject

### A couple of other solutions I entertained:
1. A bit cheeky, but I initially tried to combine multiple textures into a single one. So each 50 thumbnails, for example, would be drawn using a single texture. This would probably have helped performance; however it might've caused memory usage issues. Using this also meant having to fake Unity's Button behaviour manually.
2. The alternative would be to re-implement a scrolling component myself (listening to touch events and using OnGUI to render). However I felt that would take (for me, at least) more time than makes sense for a test.

I would also like to mention that obviously this list of 1,000 items is just a test, but in real-life my inner UI-developer would fight to add filters and categories, as 1,000 items in a single list feels wrong to me.


*This has been tested on Desktop and my Nexus 4 (I know, I know).*

***

This repository is a programming challenge by The Secret Police. To get started please **Fork** the repository into your own BitBucket account before making any commits. When you have finished your solution please create **Pull Request** to the original repository or a link to your repository.

It contains a Unity project (v5.3.4) written in C#. The test requires that you create Android builds, so please makes sure you have the Android SDK installed.

Open the Main Menu scene and run it in the IDE.
You should see a scroll view with 1000 items in that can be scrolled vertically.
Each item contains an image button and a text object.
Each image is unique on purpose.

If you do an Android build, you'll notice that the scrolling performance is really bad. We have included an FPS UI component in the Main Menu scene.

We would like you to come up with a better solution than Unity's default Scroll View.

Things to consider:
* maybe you don't need to instantiate 1000 prefabs,
* maybe you don't need all those Rect Transforms,
* minimising draw calls

We want an Android build of the Main Menu scene where it is running at 60 FPS as a minimum. We are obviously also looking at the quality of the code.

Feel free to ask us questions as you go.