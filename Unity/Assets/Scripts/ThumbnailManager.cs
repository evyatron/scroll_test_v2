﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

// Helper struct for later toggling out-of-screen objects' active state
public struct ThumbnailGameObject {
    public GameObject gameObject;
    public int index;

    public ThumbnailGameObject(GameObject gameObject, int index) {
        this.gameObject = gameObject;
        this.index = index;
    }
}

public class ThumbnailManager:MonoBehaviour {

	public Transform container;
    public GameObject prefab;
    public CanvasScaler scaler;

    private List<ThumbnailVO> _thumbnailVOList = new List<ThumbnailVO>();
    
    private ScrollRect scroller;
    private GridLayoutGroup contentLayoutGroup;
    private int numberOfRows = 0;
    private int numberOfCols = 0;
    private int rowsOnScreen = 0;
    private float totalItemHeight = 0;

    private int currentRow = 0;
    private int maxItemCreated = 0;

    // using an array - faster but makes it static (adding more items after init would be more problematic)
    // using a dictionary - iterating over all items (to toggle visibility) might be slower
    private List<ThumbnailGameObject> _thumbnailsList = new List<ThumbnailGameObject>();


    void Start() {
        scroller = GetComponentInChildren<ScrollRect>();
        contentLayoutGroup = container.GetComponent<GridLayoutGroup>();

        createThumbnailVOList();
        calculateSizes();
        createThumbnailPrefabs();
    }
	
	private void createThumbnailVOList() {
		ThumbnailVO thumbnailVO;
		for (int i=0; i<1000; i++) {
			thumbnailVO = new ThumbnailVO();
			thumbnailVO.id = i.ToString();
            _thumbnailVOList.Add(thumbnailVO);
        }
    }

    // Attached through the inspector to the ScrollView
    public void OnScroll(Vector2 data) {
        int scrollRow = Mathf.Clamp(Mathf.FloorToInt((1 - scroller.verticalNormalizedPosition) * numberOfRows), 0, numberOfRows);

        if (scrollRow != currentRow) {
            currentRow = scrollRow;

            createThumbnailPrefabs();
            toggleGameObjectsVisibility();
        }
    }

    // Create the thumbnails needed for whichever row the user is currently at
    private void createThumbnailPrefabs() {
        int itemAtTheEndOfScreen = Mathf.Clamp((currentRow + rowsOnScreen + 1) * numberOfCols, 0, _thumbnailVOList.Count);
        
        if (itemAtTheEndOfScreen > maxItemCreated) {
            for (int i = maxItemCreated; i < itemAtTheEndOfScreen; i++) {
                CreateThumbnailPrefab(i);
            }

            maxItemCreated = itemAtTheEndOfScreen;
        }
    }

    private void CreateThumbnailPrefab(int thumbnailIndex) {
        GameObject gameObj = Instantiate(prefab);
        gameObj.transform.SetParent(container, false);
        gameObj.GetComponent<Thumbnail>().thumbnailVO = _thumbnailVOList[thumbnailIndex];
        
        _thumbnailsList.Add(new ThumbnailGameObject(gameObj, thumbnailIndex));
    }

    // Make sure all items outside viewport are hidden
    private void toggleGameObjectsVisibility() {
        int row;

        for (int i = 0; i < _thumbnailsList.Count; i++) {
            row = _thumbnailsList[i].index / numberOfCols;
            _thumbnailsList[i].gameObject.SetActive(row >= currentRow && row <= currentRow + rowsOnScreen);
        }
        
        // Add padding to compensate for hidden items
        contentLayoutGroup.padding.top = Mathf.FloorToInt(currentRow * totalItemHeight);
    }

    // Calculating and caching helper numbers to be used later on during loops and events
    // Some of them can be caculated during the loops, however I find it more efficient to cache it
    // then if something changes (items or resolution) we can just call this function again
    private void calculateSizes() {
        Vector2 realScreenSize = new Vector2(Screen.width / scaler.referenceResolution.x, Screen.height / scaler.referenceResolution.y);
        float realScreenHeight = Screen.height / realScreenSize.y;

        totalItemHeight = contentLayoutGroup.cellSize.y + contentLayoutGroup.spacing.y;
        rowsOnScreen = Mathf.CeilToInt(realScreenHeight / totalItemHeight);
        numberOfCols = contentLayoutGroup.constraintCount;
        numberOfRows = _thumbnailVOList.Count / numberOfCols;
        
        RectTransform rect = container.GetComponent<RectTransform>();
        rect.sizeDelta = new Vector2(0, numberOfRows * totalItemHeight);
    }
}
